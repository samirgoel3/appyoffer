package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import Adapters.AdapterTravelList;
import appyoffer.spinno.com.appyoffer.R;
import appyoffer.spinno.com.appyoffer.SpecificDealActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by spinnosolutions on 9/1/15.
 */
public class SecondFragment extends Fragment {

    @Bind(R.id.feature_list)ListView list;
    int [] images = {R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j,R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j};



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.second_frag, container, false);

        ButterKnife.bind(this, v);
        list.setAdapter(new AdapterTravelList(getActivity(), images));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(getActivity(), SpecificDealActivity.class));
                getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });
        return v;
    }

    public static SecondFragment newInstance(String text) {

        SecondFragment f = new SecondFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}