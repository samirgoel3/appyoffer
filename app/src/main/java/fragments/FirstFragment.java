package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import Adapters.AdapterTravelList;
import appyoffer.spinno.com.appyoffer.R;
import appyoffer.spinno.com.appyoffer.SpecificDealActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by spinnosolutions on 9/1/15.
 */
public class FirstFragment extends Fragment {

    @Bind(R.id.travel_list) ListView list;
    int [] images = {R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e};




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.first_frag, container, false);
        ButterKnife.bind(this, v);
        list.setAdapter(new AdapterTravelList(getActivity(), images));

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(getActivity() , SpecificDealActivity.class));
                getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });

        return v;
    }

    public static FirstFragment newInstance(String text) {

        FirstFragment f = new FirstFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}