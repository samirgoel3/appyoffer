package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import Adapters.AdapterTravelList;
import appyoffer.spinno.com.appyoffer.R;
import appyoffer.spinno.com.appyoffer.SpecificDealActivity;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by spinnosolutions on 9/1/15.
 */
public class ThirdFragment extends Fragment {




    @Bind(R.id.product_list)ListView list;
    int [] images = {R.drawable.k,R.drawable.l,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p,R.drawable.g,R.drawable.k,R.drawable.l,R.drawable.m,R.drawable.n,R.drawable.o,R.drawable.p};



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.third_frag, container, false);
        ButterKnife.bind(this, v);
        list.setAdapter(new AdapterTravelList(getActivity(), images));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                startActivity(new Intent(getActivity(), SpecificDealActivity.class));
                getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
            }
        });
        return v;
    }

    public static ThirdFragment newInstance(String text) {

        ThirdFragment f = new ThirdFragment();
        Bundle b = new Bundle();
        b.putString("msg", text);

        f.setArguments(b);

        return f;
    }
}