package appyoffer.spinno.com.appyoffer;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SpecificDealActivity extends Activity {
    @Bind(R.id.im1)ImageView image1 ;
    @Bind(R.id.im2)ImageView image2 ;
    @Bind(R.id.im3)ImageView image3 ;
    @Bind(R.id.im4)ImageView image4 ;
    @Bind(R.id.im5)ImageView image5 ;
    @Bind(R.id.back)LinearLayout back_btn ;

    @Bind(R.id.main_img)ImageView mainimage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specific_deal);
        ButterKnife.bind(this);


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        mainimage.setImageResource(R.drawable.e);

        image1.setImageResource(R.drawable.room_img_one);
        image2.setImageResource(R.drawable.room_img_two);
        image3.setImageResource(R.drawable.room_img_three);
        image4.setImageResource(R.drawable.room_img_four);
        image5.setImageResource(R.drawable.room_img_five);
    }
}
