package appyoffer.spinno.com.appyoffer;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;

import net.simonvt.menudrawer.MenuDrawer;
import net.simonvt.menudrawer.Position;

import butterknife.Bind;
import butterknife.ButterKnife;
import fragments.FirstFragment;
import fragments.SecondFragment;
import fragments.ThirdFragment;

public class MainActivity extends FragmentActivity {



    @Bind(R.id.menu_opnelllr)LinearLayout menu_opner ;


    /////////////////////  menu attributes
    @Bind(R.id.browse_deals_menu_btn)LinearLayout browse_deals_menu_btn ;
    @Bind(R.id.my_purchase_menu_btn)LinearLayout my_purchase_menu_btn ;
    @Bind(R.id.payment_menu_btn)LinearLayout payment_menu_btn ;
    @Bind(R.id.my_subscription_menu_btn)LinearLayout my_subscription_menu_btn ;
    @Bind(R.id.settings_menu_btn)LinearLayout settings_menu_btn ;
    @Bind(R.id.about_menu_btn)LinearLayout about_menu_btn ;
    @Bind(R.id.login_or_signup_btn)LinearLayout login_or_signup_btn ;


    private MenuDrawer mDrawer;
    ViewPager pager ;
    PagerSlidingTabStrip tabs ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDrawer = MenuDrawer.attach(this , Position.LEFT);
        mDrawer = MenuDrawer.attach(this);
        mDrawer.setContentView(R.layout.activity_main);
        mDrawer.setMenuView(R.layout.main_menu_drawer);
        ButterKnife.bind(this);

        pager = (ViewPager) findViewById(R.id.viewPager);
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);


        pager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));
        tabs.setViewPager(pager);
        tabs.setTextColor(Color.WHITE);
        tabs.setIndicatorHeight(3);
        tabs.setIndicatorColor(Color.WHITE);



        menu_opner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.toggleMenu();
            }
        });



         browse_deals_menu_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 mDrawer.closeMenu();
             }
         });



        my_subscription_menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.closeMenu();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, MySubscriptionActivity.class));
                        overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                    }
                }, 700);

            }
        });


        login_or_signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawer.closeMenu();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, LoginSignUpActivity.class));
                        overridePendingTransition(R.anim.pull_in_right, R.anim.no_sliding);
                    }
                }, 700);
            }
        });



    }






    private class MyPagerAdapter extends FragmentPagerAdapter {
        private final String[] TITLES = { "Travel", "Feature", "Products" };

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch(pos) {

                case 0: return FirstFragment.newInstance("FirstFragment, Instance 1");
                case 1: return SecondFragment.newInstance("SecondFragment, Instance 1");
                case 2: return ThirdFragment.newInstance("ThirdFragment, Instance 1");
                default: return ThirdFragment.newInstance("ThirdFragment, Default");
            }
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }


    }





}
