package Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import appyoffer.spinno.com.appyoffer.R;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by spinnosolutions on 9/1/15.
 */
public class AdapterTravelList  extends BaseAdapter {


    Context con ;
    LayoutInflater inflater ;
    int [] images ;

    public AdapterTravelList( Context con , int [] images){
        this.con =  con ;
        this.images = images ;
        inflater  = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_list, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.image.setImageResource(images[position]);




        return view;
    }

    static class ViewHolder {
        @Bind(R.id.image) ImageView image;


        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
